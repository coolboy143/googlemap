<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CY Google Map</title>
    <style>
        #map{
            height:400px;
            width:100%;
        }
    </style>

</head>
<body>
<h1>CY Google Map</h1>
<div id="map"></div>
<script>
    function initMap()
    {
        var options={
         zoom:8,
         center:{lat:28.3949,lng:-84.1240}
        }
        var map=new google.maps.Map(document.getelementById('map'),options)
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key= \tAIzaSyC0DpOuePPEkmWQWcZTmiItp1XY01EQ4LM&callback=initMap"
async defer></script>


</body>
</html>